$(document).ready(function () {
    $('.carousel').carousel({
        interval: 15000
    });

    $('.carousel').carousel('cycle');
});

$(function() {
    $('.password').pstrength();
});

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({
        placement: 'left'
    });
    $('[data-toggle="tooltip-me"]').tooltip({
        placement: 'top'
    });
    $('[data-toggle="tooltip-bt"]').tooltip({
        placement: 'bottom'
    });
});

$(function() {
    $(".dropdown").hover(function() {
        $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
        $(this).toggleClass('open');
        $('b', this).toggleClass("caret caret-up");
    }, function() {
        $('.dropdown-menu', this).stop(true, true).fadeOut("fast");
        $(this).toggleClass('open');
        $('b', this).toggleClass("caret caret-up");
    });
});

$(function() {
    $('#accordion-toggle-01, #accordion-toggle-02, #accordion-toggle-03, #accordion-toggle-04, #accordion-toggle-05, #accordion-toggle-06, #headingOne, #headingTwo, #headingThree, #dayOne, #dayTwo, #dayThree').on("click", function() {
        $(this).find('i').toggleClass('glyphicon-menu-down').toggleClass('glyphicon-menu-up');
    });
});

$('.more').click(function() {
    $('.oculta').slideToggle();
});

$(document).ready(function(){
	$('#buttomcafedamanhatropical').click(function(){
		$("#buttomcafedamanhatropical").toggleClass('click-button-close');
		$("#box-cafedamanhatropical").toggleClass('dt-row-01-active');
		if($(this).text() == 'FECHAR'){
			$(this).text('OUTRAS OPÇÕES');
			} else {
			$(this).text('FECHAR');
		}
	});
	$('#buttomcoffeebreakmanha').click(function(){
		$("#box-coffeebreakmanha").toggleClass('dt-row-01-active');
		if($(this).text() == 'FECHAR'){
			$(this).text('OUTRAS OPÇÕES');
			} else {
			$(this).text('FECHAR');
		}
	});
	$('#buttomalmocobuffetitaliano').click(function(){
		$("#box-almocobuffetitaliano").toggleClass('dt-row-01-active');
		if($(this).text() == 'FECHAR'){
			$(this).text('OUTRAS OPÇÕES');
			} else {
			$(this).text('FECHAR');
		}
	});
	$('#buttomcoffeebreaktarde').click(function(){
		$("#box-coffeebreaktarde").toggleClass('dt-row-01-active');
		if($(this).text() == 'FECHAR'){
			$(this).text('OUTRAS OPÇÕES');
			} else {
			$(this).text('FECHAR');
		}
	});
	$('#buttomjantaralacartemarroquino').click(function(){
		$("#box-jantaralacartemarroquino").toggleClass('dt-row-01-active');
		if($(this).text() == 'FECHAR'){
			$(this).text('OUTRAS OPÇÕES');
			} else {
			$(this).text('FECHAR');
		}
	});
});

$(window).load(function(){
	var totalItems = $('#carouselgaleria .carousel-inner .item').length;
	var currentIndex = $('div.active').index() + 1;
	$('.num').html(''+currentIndex+' - '+totalItems+'');
	
	$('#carouselgaleria').bind('slid.bs.carousel', function() {
		currentIndex = $('div.active').index() + 1;
	   $('.num').html(''+currentIndex+' - '+totalItems+'');
	});
});

$(window).load(function(){
	$('.show-time').countdown('2016/05/01 09:00', function(event) {
		$(this).html(event.strftime('%D dias %H:%M:%S'));
	});
});

$(document).ready(function(){
	$('#toggle_event_editing button').click(function(){
		if($(this).hasClass('locked_active') || $(this).hasClass('unlocked_inactive')){
			/* code to do when unlocking */
			$('#switch_status').html('Publicado.');
		}else{
			/* code to do when locking */
			$('#switch_status').html('Não publicado.');
		}
		
		/* reverse locking status */
		$('#toggle_event_editing button').eq(0).toggleClass('locked_inactive locked_active btn-default btn-info');
		$('#toggle_event_editing button').eq(1).toggleClass('unlocked_inactive unlocked_active btn-info btn-default');
	});
});