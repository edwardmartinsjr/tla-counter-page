$(document).ready(function(){

	var map;        
	var myCenter=new google.maps.LatLng(-23.5406, -46.632);
	var marker=new google.maps.Marker({
		position:myCenter
	});
	
	function initialize() {
	  var mapProp = {
		  center:myCenter,
		  zoom: 14,
		  draggable: true,
		  scrollwheel: true,
		  mapTypeId:google.maps.MapTypeId.ROADMAP
	  };
	  
	  map=new google.maps.Map(document.getElementById("map_canvas"),mapProp);
	  marker.setMap(map);
		
	  google.maps.event.addListener(marker, 'click', function() {
		  
		infowindow.setContent(contentString);
		infowindow.open(map, marker);
		
	  }); 
	};
	google.maps.event.addDomListener(window, 'load', initialize);
	
	google.maps.event.addDomListener(window, "resize", resizingMap());
	
	$('#mapa').on('show.bs.modal', function() {
	   resizeMap();
	})
	
	function resizeMap() {
	   if(typeof map =="undefined") return;
	   setTimeout( function(){resizingMap();} , 400);
	}
	
	function resizingMap() {
	   if(typeof map =="undefined") return;
	   var center = map.getCenter();
	   google.maps.event.trigger(map, "resize");
	   map.setCenter(center); 
	}
});